require 'test_helper'

class StudentsControllerTest < ActionController::TestCase
  test "should get login_failed" do
    get :login_failed
    assert_response :success
  end

  test "should get login_success" do
    get :login_success
    assert_response :success
  end

end
